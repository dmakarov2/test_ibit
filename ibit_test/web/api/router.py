from fastapi.routing import APIRouter

from ibit_test.web.api import websockets

api_router = APIRouter()
api_router.include_router(websockets.router)
