import datetime
from typing import Any, Dict, List, Union

from pydantic import BaseModel


class IbitRequest(BaseModel):
    """Default request schema."""

    action: str
    message: Dict[str, Any] = {}


class IbitResponse(BaseModel):
    """Default response schema."""

    action: str
    message: Union[Dict[str, Any], List[Any]] = {}


class AssetsSchema(BaseModel):
    """Schema for assets response."""

    assets: List[Dict[str, Union[str, int]]]


class LastHalfAnHourAssetsSchema(BaseModel):
    """Schema for assets for the last half hour."""

    points: List[Dict[str, Union[str, int, datetime.datetime]]]
