import logging

from fastapi import APIRouter, WebSocket, WebSocketDisconnect

from ibit_test.web.api.websockets.handler import handle_request
from ibit_test.web.api.websockets.manager import ws_manager

router = APIRouter()
logger = logging.getLogger(__name__)


@router.websocket("/ws")
async def ws_connection(websocket: WebSocket) -> None:
    """
    Establish connection with the server.

    :param websocket: websocket connection.
    """
    client_addr = f"{websocket.client.host}:{websocket.client.port}"  # type: ignore
    await websocket.accept()
    ws_manager.add_client(
        websocket=websocket,
        connection_id=client_addr,
    )
    while True:
        try:
            data = await websocket.receive_text()
            await handle_request(
                data,
                client_addr,
                websocket,
            )
        except WebSocketDisconnect:
            ws_manager.remove_client(
                connection_id=client_addr,
            )
            break
        except Exception as exc:
            logger.warning(exc)
            ws_manager.remove_client(
                connection_id=client_addr,
            )
            break
