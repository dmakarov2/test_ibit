"""Package to work with websockets."""

from ibit_test.web.api.websockets.views import router

__all__ = ["router"]
